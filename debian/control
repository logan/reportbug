Source: reportbug
Section: utils
Priority: standard
Maintainer: Reportbug Maintainers <debian-reportbug@lists.debian.org>
Uploaders: Sandro Tosi <morph@debian.org>, Nis Martensen <nis.martensen@web.de>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 12), python3, dh-python
Build-Depends-Indep: python3-pytest, python3-pytest-cov, python3-setuptools
Vcs-Git: https://salsa.debian.org/reportbug-team/reportbug.git
Vcs-Browser: https://salsa.debian.org/reportbug-team/reportbug
Rules-Requires-Root: no

Package: reportbug
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, apt, python3-reportbug (= ${source:Version}), sensible-utils
Suggests: default-mta | postfix | exim4 | mail-transport-agent, gnupg | pgp, debconf-utils (>> 1.1.0), debsums (>= 2.0.47), file (>> 1.30), dlocate, python3-urwid, reportbug-gtk (= ${source:Version}), xdg-utils, emacs-bin-common, claws-mail (>= 3.8.0)
Description: reports bugs in the Debian distribution
 reportbug is a tool designed to make the reporting of bugs in Debian
 and derived distributions relatively painless.  Its features include:
 .
  * Integration with many mail user agents.
  * Access to outstanding bug reports to make it easier to identify
    whether problems have already been reported.
  * Automatic checking for newer versions of packages.
  * Optional automatic verification of integrity of packages via debsums.
  * Support for following-up on outstanding reports.
  * Optional PGP/GnuPG integration.
 .
 Bug reporting in Debian relies on email; reportbug can use a local
 mail transport agent (like exim or sendmail), submit directly through
 an external mail server, or pass messages to an installed mail user
 agent (e.g., mutt) for submission.
 .
 This package also includes the "querybts" script for browsing the
 Debian bug tracking system.

Package: python3-reportbug
Section: python
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, apt, python3-debian, python3-debianbts (>= 2.10), file, python3-requests, python3-apt, sensible-utils
Suggests: reportbug
Description: Python modules for interacting with bug tracking systems
 reportbug is a tool designed to make the reporting of bugs in Debian
 and derived distributions relatively painless.
 .
 This package includes Python modules which may be reusable by other
 tools that want to interact with the Debian bug tracking system.
 .
 To actually report a bug, install the reportbug package.

Package: reportbug-gtk
Architecture: all
Priority: optional
Depends: ${misc:Depends}, reportbug (= ${source:Version}), python3-gi, python3-gi-cairo, gir1.2-gtk-3.0, gir1.2-vte-2.91, gir1.2-gtksource-3.0, python3-gtkspellcheck
Description: reports bugs in the Debian distribution (GTK+ UI)
 reportbug is a tool designed to make the reporting of bugs in Debian
 and derived distributions relatively painless.  Its features include:
 .
  * Integration with many mail user agents.
  * Access to outstanding bug reports to make it easier to identify
    whether problems have already been reported.
  * Automatic checking for newer versions of packages.
  * Optional automatic verification of integrity of packages via debsums.
  * Support for following-up on outstanding reports.
  * Optional PGP/GnuPG integration.
 .
 Bug reporting in Debian relies on email; reportbug can use a local
 mail transport agent (like exim or sendmail), submit directly through
 an external mail server, or pass messages to an installed mail user
 agent (e.g., mutt) for submission.
 .
 This package contains a desktop file and icon, and has dependencies
 to enable the GTK+ UI mode of reportbug to work.
